# ActiveMQ Artemis Docker

## Base Image

[vromero/activemq-artemis](https://hub.docker.com/r/vromero/activemq-artemis)

## .env

```bash
cp .env.example .env
```

```.env
### Drivers ################################################
VOLUMES_DRIVER=local
NETWORKS_DRIVER=bridge

### ActiveMQ Artemis ################################################
ARTEMIS_PORT=61616
ARTEMIS_SSL_PORT=61617
ARTEMIS_WEB_ADMIN_PORT=8161
```

## PKCS#12

```
cp your_broker.p12 activemq_artemis/certs/broker.p12
```

The activemq-artemis docker runs as 'artemis' instead of 'root', so make sure the p12 file is readable by 'artemis'.

## Build

Once activemq_artemis/broker.xml is modified, remember to exec 'docker-compose build --no-cache' to make docker image fresh.

```bash
./ctl.sh build
```

## Run

```bash
./ctl.sh start
```

## Commands

default user is "artemis".

default password is "simetraehcapa".

### User List

```bash
sudo docker-compose exec activemq-artemis ./artemis user list --user artemis --password simetraehcapa
```

```bash
sudo docker exec artemis ./artemis user list --user artemis --password simetraehcapa
```

#### For 2.9-alpine-latest

```bash
sudo docker exec artemis ./artemis user list
```

### User Add

```bash
sudo docker-compose exec activemq-artemis ./artemis user add --user artemis --password simetraehcapa --role amq --user-command-user {user} --user-command-password {password}
```

```bash
sudo docker exec artemis ./artemis user add --user artemis --password simetraehcapa --role amq --user-command-user {user} --user-command-password {password}
```

#### For 2.9-alpine-latest

```bash
sudo docker exec artemis ./artemis user add --user {user} --password {password} --role amq
```

### User Remove

```bash
sudo docker-compose exec activemq-artemis ./artemis user rm --user artemis --password simetraehcapa --user-command-user {user}
```

```bash
sudo docker exec artemis ./artemis user rm --user artemis --password simetraehcapa --user-command-user {user}
```

#### For 2.9-alpine-latest

```bash
sudo docker exec artemis ./artemis user rm --user {user}
```

