#!/bin/bash
set -e

case "$1" in
        start)
                sudo docker-compose up -d
                ;;
        stop)
                sudo docker-compose stop
                ;;
        restart)
                sudo docker-compose restart
                ;;
        down)
                sudo docker-compose down
                ;;
        build)
                sudo docker-compose build --no-cache
                ;;
        purge)
                sudo docker-compose down --volumes
                ;;
        status)
                sudo docker-compose ps
                ;;
        ufw-docker)
                sudo ufw-docker delete allow artemis
                sudo ufw-docker allow artemis 61616
                sudo ufw-docker allow artemis 61617
                ;;
        *)
                echo -e "Start service："
                echo -e "\t$0 start"
                echo
                echo -e "Stop service："
                echo -e "\t$0 stop"
                echo
                echo -e "Restart service："
                echo -e "\t$0 restart"
                echo
                echo -e "Servie down："
                echo -e "\t$0 down"
                echo
                echo -e "Build Image："
                echo -e "\t$0 build"
                echo
                echo -e "Purge："
                echo -e "\t$0 purge"
                echo
                echo -e "Service status："
                echo -e "\t$0 status"
                ;;
esac
